export class StorageUtil {
    public static localStorageSave<T>(key: string, value: T): void {
        localStorage.setItem(key, JSON.stringify(value));
    }

    public static localStorageDelete(key: string): void {
        localStorage.removeItem(key);
    }
    
    public static localStorageRead<T>(key: string): T | undefined {
        const storedValue = localStorage.getItem(key);
        try {
            // If the storedValue is there, return and parse it as a JSON object.
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            }
    
            return undefined;   
        }
        // If there is an errror, remove the item from storage and return undefined.
        catch (error) {
            localStorage.removeItem(key);
            return undefined;
            
        }
    }

    public static sessionStorageSave<T>(key: string, value: T): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    }

    public static sessionStorageDelete(key: string): void {
        localStorage.removeItem(key);
    }
    
    public static sessionStorageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        try {
            // If the storedValue is there, return and parse it as a JSON object.
            if (storedValue) {
                return JSON.parse(storedValue) as T;
            }
    
            return undefined;   
        }
        // If there is an errror, remove the item from storage and return undefined.
        catch (error) {
            sessionStorage.removeItem(key);
            return undefined;
            
        }
    }
}
