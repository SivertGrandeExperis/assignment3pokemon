import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent {

  @Input() pokemon: Pokemon[] = [];

  constructor(
    private router: Router,
    private userService: UserService
  ) {}

  getRouterUrl(): string {
    return this.router.url;
  }

  getUserService(): UserService {
    return this.userService;
  }
}
