import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { CollectionService } from 'src/app/services/collection.service';

@Component({
  selector: 'app-remove-pokemon',
  templateUrl: './remove-pokemon.component.html',
  styleUrls: ['./remove-pokemon.component.css']
})
export class RemovePokemonComponent{
  @Input() monsterName: string = '';
  public loading: boolean = false;

  constructor(
    private readonly collectionService: CollectionService
  ) {}

  onRemoveButtonClick(): void {
    // add catched Pokémon to the Trainer page
    this.loading = true;
    this.collectionService.removeFromCollection(this.monsterName).subscribe({
      next: () => {},
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message)
      }
    });
  }
}
