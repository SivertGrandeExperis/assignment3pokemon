import { Component, EventEmitter, Output } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();

  // Dependency injection
  constructor(
    private readonly loginService: LoginService,
    private readonly userService: UserService,
    ) { }

  /**
   * Method to handle the submission of a login form.
   * @param loginForm Argument that is an instance of the NgForm class.
   */
  public loginSubmit(loginForm: NgForm): void {

    // Extracting the username from the login form.
    const { username } = loginForm.value;
    
    this.loginService.login(username)
      .subscribe({
        next: (trainer: Trainer) => {
          this.userService.trainer = trainer;
          this.login.emit();
        },
        error: () => {

        }
      })
  }
}
