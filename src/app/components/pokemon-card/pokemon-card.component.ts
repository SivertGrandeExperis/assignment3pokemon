import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { UserService } from 'src/app/services/user.service';

const SpritesBaseUrl: string = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.css']
})
export class PokemonCardComponent {

  @Input() monster!: Pokemon;

  // Dependency injection.
  constructor(
    private router: Router,
    private userService: UserService
  ) {}
  
  // Method that retuns the current URL of the Angular router.
  getRouterUrl(): string {
    return this.router.url;
  }

  // Method that returns an instance of the UserService class.
  getUserService(): UserService {
    return this.userService;
  }

  /**
   * Method for extracting the id from a url for a specific Pokémon.
   * @returns An id of type number which belongs to a Pokémon.
   */
  extractIdFromUrl(): number {
    if (!this.monster) return 0;
    const urlParts = this.monster.url.split("/");
    return parseInt(urlParts[6])
  }

  // Method for retrieving the sprite image for a specific Pokémon.
  getSprite(): string {
    return `${SpritesBaseUrl}${this.extractIdFromUrl()}.png`
  }
}
