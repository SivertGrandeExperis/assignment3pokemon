import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectionService } from 'src/app/services/collection.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-catch-pokemon',
  templateUrl: './catch-pokemon.component.html',
  styleUrls: ['./catch-pokemon.component.css'],
})
export class CatchPokemonComponent implements OnInit {
  @Input() monsterName: string = '';
  public isCollected: boolean = false;
  public loading: boolean = false;

  constructor(
    private userService: UserService,
    private readonly collectionService: CollectionService
  ) {}

  ngOnInit(): void {
    // Check if pokemon is collected or not
    this.isCollected = this.userService.inCollection(this.monsterName);
  }

  /**
   * Functionality for adding a pokemon to the collection upon clicking the button.
   */
  onCatchButtonClick(): void {
    this.loading = true;
    this.collectionService.addToCollection(this.monsterName).subscribe({
      next: (trainer: Trainer) => {
        this.isCollected = this.userService.inCollection(this.monsterName)
      },
      error: (error: HttpErrorResponse) => {
        console.log("ERROR", error.message)
      }
    });
  }
}
