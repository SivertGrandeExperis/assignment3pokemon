import { Component } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { LoginService } from 'src/app/services/login.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {

  get trainer(): Trainer | undefined {
    return this.userService.trainer
  }

  constructor(
    private readonly userService: UserService,
    private readonly loginService: LoginService,
  ) { }

  onLogOutClick() {
    this.loginService.logout();
    window.location.reload();
  }
}
