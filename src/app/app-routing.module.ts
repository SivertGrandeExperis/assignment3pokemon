import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login" // when path is empty, redirect to login page.
  },
  {
    path: "login",
    component: LoginPage
  },
  {
    path: "trainer",
    component: TrainerPage,
    canActivate: [ AuthGuard ] // Only authenticated users that are logged in get access to TrainerPage.
  },
  {
    path: "catalogue",
    component: CataloguePage,
    canActivate: [ AuthGuard ] // Only authenticated users that are logged in get access to CataloguePage.
  }
];

// Configuring the RouterModule
@NgModule({
  imports: [RouterModule.forRoot(routes)],  // Import a module
  exports: [RouterModule]  // Expose module and it's features
})
export class AppRoutingModule { }
