// Interface which defines a model that represents the data stored in an API.
// The results property of this object contains an array of Pokemon objects.
export interface PokemonData {
    results: Pokemon[];
}

// Interface which defines the shape of a Pokemon object.
export interface Pokemon {
    name: string;
    url: string;
}