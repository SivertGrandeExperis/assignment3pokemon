// Interface which defines the shape of a Trainer object.
export interface Trainer {
    id: number;
    username: string;
    pokemon: string[];
}