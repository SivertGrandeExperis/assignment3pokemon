import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { CatalogueService } from 'src/app/services/catalogue.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit{

  get trainer(): Trainer | undefined {
    return this.userService.trainer;
  }

  get pokemon(): Pokemon[] {
    if (this.userService.trainer) {
      return this.catalogueService.pokemon;
    }

    return []
  }

  constructor(
    private readonly catalogueService: CatalogueService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.catalogueService.findAllPokemon();
  }
}
