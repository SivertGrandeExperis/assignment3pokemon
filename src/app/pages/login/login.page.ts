import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage {
  
  constructor(private readonly router: Router) { }

  // When user is logged in, redirect to the catalogue page.
  handleLogin(): void {
    this.router.navigateByUrl("/catalogue");
  }

}
