import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { StorageKeys } from '../enums/storage-keys.enum';
import { PokemonData, Pokemon } from '../models/pokemon.model';
import { StorageUtil } from '../utils/storage.util';

const pokemonUrl: string = 'https://pokeapi.co/api/v2/pokemon?limit=151';

@Injectable({
  providedIn: 'root',
})
export class CatalogueService {
  // Storing the pokemons in an array which is initialized as an empty array.
  private _pokemon: Pokemon[] = [];
  private _error: string = '';
  private _loading: boolean = false;

  // Exposing getter functions
  get pokemon(): Pokemon[] {
    return this._pokemon;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  /**
   * Method which fethces the first 151 Pokémons from the PokeAPI.
   * Once all the Pokémon are fetched, they will be stored in sessionStorage.
   * This means we can read from sessionStorage instead of constantly having to fetch from the API.
   */
  public findAllPokemon(): void {
    this._loading = true;
    const pokeStorage = StorageUtil.sessionStorageRead<Pokemon[]>(StorageKeys.Pokemon);

    if (!pokeStorage) {
      console.log("fetching from pokeapi")
      // If there are no pokemon in session storage, fetch them
      this.http.get<PokemonData>(pokemonUrl)
        .pipe(
          finalize(() => {
            this._loading = false;
          })
        )
        .subscribe({
          next: (pokemonData: PokemonData) => {
            this._pokemon = pokemonData.results;
            StorageUtil.sessionStorageSave<Pokemon[]>(StorageKeys.Pokemon, this._pokemon);
          },
          error: (error: HttpErrorResponse) => {
            this._error = error.message;
          },
        });
    }
    else {
      this._pokemon = pokeStorage;
      this._loading = false;
    }
  }

  /**
   * Function to find a pokemon by name. 
   * If the name is found then return the name, else return undefined.
   */
  public pokemonByName(monsterName: string): Pokemon | undefined {
    return this._pokemon.find((monster: Pokemon) => monster.name === monsterName);
  }
}
