import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

const { apiTrainers, apiKey } = environment;

@Injectable({
  providedIn: 'root',
})

export class LoginService {
  // Dependency injection.
  constructor(
    private readonly http: HttpClient,
    private readonly router: Router
  ) { };

  /**
   * Function for logging in
   * @param username Takes in a username parameter of type string
   * @returns 
   */
  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username)
    .pipe(
      switchMap((trainer: Trainer | undefined) => {
        // If a trainer does not exist, a new user will be created.
        if (trainer === undefined) {
          return this.createUser(username);
        }
        // If a user already exists, return an Observable that emits the trainer object.
        return of(trainer);
      })
    )
  };

    /**
   * Function for logging out
   */
    public logout(): void {
      StorageUtil.localStorageDelete(StorageKeys.Trainer);
      this.router.navigate([""]);
    };

  /**
   * Check if user already exists.
   * @param username Takes in a username parameter of type string
   * @returns an Observable which makes an HTTP GET request and filters the response by username, 
   * and returns the last 'Trainer' object in an array of 'Trainer' objects.
   */
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`).pipe(
      map((response: Trainer[]) => response.pop())
    );
  }

  /**
   * Create a user
   * @param username Takes in a username parameter of type string
   * @returns A HTTP POST request
   */
  private createUser(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: [],
    };

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'x-api-key': apiKey,
    });

    return this.http.post<Trainer>(apiTrainers, trainer, {
      headers,
    });
  }
}
