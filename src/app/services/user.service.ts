import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.localStorageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() { 
    this._trainer = StorageUtil.localStorageRead<Trainer>(StorageKeys.Trainer);
  }

  public inCollection(monsterName: string): boolean {
    if (this._trainer) {
      return Boolean(
        this.trainer?.pokemon.find(
          (monster: string) => monster === monsterName));
    }
    return false;
  }

  /**
   * Method to remove a Pokémon from the collection of the currently logged in Trainer.
   * @param monsterName Parameter, which is the name of the Pokémon to be removed.
   */
  public removeFromCollection(monsterName: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((monster: string) => monster !== monsterName);
    }
  }

  /**
   * Method to add a Pokémon from the collection of the currently logged in Trainer.
   * @param monster Parameter, which is the name of the Pokémon to be added.
   */
  public addToCollection(monster: Pokemon): void {
    if (this._trainer) {
      this._trainer.pokemon.push(monster.name);
    }
  }
}
