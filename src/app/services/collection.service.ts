import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { CatalogueService } from './catalogue.service';
import { UserService } from './user.service';


const { apiKey, apiTrainers } = environment;

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly catalogueService: CatalogueService,
    private readonly userService: UserService,
  ) { }
  
  /**
   * Method for adding a Pokémon to the Trainer's collection of caught Pokémon.
   * After the Pokémon is added to the collection, there will be sent a PATCH request
   * to the API, which updates the 'pokemon' property for the current trainer that is logged in.
   * @param monsterName Parameter, which represents the name of the Pokémon to be added.
   * @returns An Observable of type 'Trainer'.
   */
  public addToCollection(monsterName: string): Observable<Trainer> {
    if (!this.userService.trainer) {
      throw new Error("AddToCollection: There is no trainer")
    }

    const trainer: Trainer = this.userService.trainer;
    const monster: Pokemon | undefined = this.catalogueService.pokemonByName(monsterName)

    if (!monster) {
      throw new Error("No monster with name: " + monsterName);
    }

    // Check if pokemon exists in the trainer's collection
    if (!this.userService.inCollection(monsterName)) {
      this.userService.addToCollection(monster);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    this._loading = true;

    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon] // Already updated
    }, {
      headers
    })
    .pipe(
      tap((updatedtrainer: Trainer) => {
        this.userService.trainer = updatedtrainer;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }

  public removeFromCollection(monsterName: string): Observable<Trainer> {
    if (!this.userService.trainer) {
      throw new Error("AddToCollection: There is no trainer")
    }

    const trainer: Trainer = this.userService.trainer;
    const monster: Pokemon | undefined = this.catalogueService.pokemonByName(monsterName)

    if (!monster) {
      throw new Error("No monster with name: " + monsterName);
    }

    // Check if pokemon exists in the trainer's collection
    if (this.userService.inCollection(monsterName)) { 
      this.userService.removeFromCollection(monsterName);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    this._loading = true;

    return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`, {
      pokemon: [...trainer.pokemon] // Already updated
    }, {
      headers
    })
    .pipe(
      tap((updatedtrainer: Trainer) => {
        this.userService.trainer = updatedtrainer;
      }),
      finalize(() => {
        this._loading = false;
      })
    )
  }
}
