// Used when interacting with session or local storage, either by reading
// or saving from storage. By doing so, we can eliminate magic strings.
export enum StorageKeys{
   Trainer = "trainer-user",
   Pokemon = "pokemon-monster"
}