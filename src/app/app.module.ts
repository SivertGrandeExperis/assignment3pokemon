import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { TrainerPage } from './pages/trainer/trainer.page';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PokemonCardComponent } from './components/pokemon-card/pokemon-card.component';
import { RemovePokemonComponent } from './components/remove-pokemon/remove-pokemon.component';
import { CatchPokemonComponent } from './components/catch-pokemon/catch-pokemon.component';

@NgModule({
  declarations: [
    // Components
    AppComponent,
    LoginPage,
    TrainerPage,
    CataloguePage,
    LoginFormComponent,
    NavbarComponent,
    PaginationComponent,
    PokemonCardComponent,
    RemovePokemonComponent,
    CatchPokemonComponent,
  ],
  imports: [
    // Modules
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
