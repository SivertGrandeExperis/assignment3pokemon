# Assignment 3: Creating a Pokémon Trainer with Angular

## Description

In this assignment we had to build a Pokémon Trainer web app as a Single Page Application using the Angular Framework. We are using two API's where one of them is hosted locally through Railway, while the latter is publicly available at the following link: https://pokeapi.co/.

On the web app you are able to log in as a Pokémon Trainer simply by entering a name. If there is no existing Trainer with the entered name in the API a new user will be created. Otherwise, you will be able to log into an existing account. 

Once you are logged in, you will be able to browse through a catalogue of 1st generation Pokémon, and you will be able to catch them and add them to your collection. You can also visit your Trainer page, which will display all the Pokémon that you have caught. Here you also have the opportunity to remove some Pokémon from your collection.

## Install

To install the application you would need to to download Node.js, the Angular CLI, as well as git.

If you want to run the application on a local machine you will need to get access to the environment variables used in this project, which are stored in an environments.ts file. Otherwise, you would need to host the Pokémon Trainer API yourself.

## Usage

After you are done with the installation you can start up the server and open the browser by typing the following command in the terminal:

```shell
ng serve -o
```


## Contributors
This assignment was developed by [Sivert Grande](https://gitlab.com/SivertGrandeExperis) and [Tore Kamsvåg](https://gitlab.com/Torekamsvag)
